part of 'models.dart';

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate});

  @override
  // TODO: implement props
  List<Object> get props =>
      [id, picturePath, description, ingredients, price, rate];
}

List<Food> mockFoods = [
  Food(
      id: 1,
      picturePath: "assets/food/b1.jpg",
      name: "Kue Gulung",
      description: "Jajan Pasar",
      ingredients: "Tepung, Telur, Selai",
      price: 15000,
      rate: 4.2),
  Food(
      id: 2,
      picturePath: "assets/food/b2.jpg",
      name: "Gethuk Goreng",
      description: "Makanan khas lebak",
      ingredients: "Singkong, Gula merah, tepung",
      price: 20000,
      rate: 4.4),
  Food(
      id: 3,
      picturePath: "assets/food/b3.jpg",
      name: "Donat",
      description: "Makanan",
      ingredients: "Tepung, telur, seres",
      price: 25000,
      rate: 4.9),
  Food(
      id: 4,
      picturePath: "assets/food/b4.jpg",
      name: "Roti Rainbow",
      description: "Roti lapis pelangi",
      ingredients: "selai, roti",
      price: 20000,
      rate: 4.5),
  
];
